<?php
$location = realpath(dirname(__FILE__));
require_once $location . '/decrement_large_number_v1.php';
$i = '9223372036854775807';
do {
	$i = decrement_large_number_v1($i, TRUE);
	echo "$i\n";
} while ($i != '0');
?>